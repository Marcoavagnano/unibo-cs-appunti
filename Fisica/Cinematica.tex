\documentclass[11pt]{article}
\usepackage[a4paper]{geometry}
\usepackage[utf8]{inputenc} % Accenti
\usepackage{enumitem} % Enumerazione con lettere
\usepackage{tikz} % Grafici
\usetikzlibrary{arrows, calc, angles, quotes} % Grafici
\usepackage{pgfplots} % Grafici
\pgfplotsset{compat=newest} % Usiamo la versione più recente di pgfplots, Overleaf si arrabbia altrimenti
\usepackage{amssymb} % Simboli matematici
\usepackage{nccmath} % Comandi matematici estesi
\usepackage{cancel} % Simbolo per la cancellazione

\title{Fisica}
\begin{document}

\section{Cinematica}

\subsection{Moto in 1 dimensione}
\begin{center}
    \begin{tikzpicture}[scale=0.8]
        \begin{axis}[
                axis x line = center,
                axis y line = center,
                xtick = {0},
                ytick = {30},
                xticklabels = {},
                yticklabels = {$ s_{0} $},
                xlabel = {$ t $},
                ylabel = {$ s $},
                xlabel style = {below right},
                ylabel style = {above left},
                xmin = 0, xmax = 120,
                ymin = 0, ymax = 90,
                enlargelimits = true
            ]
            \addplot[smooth] coordinates{ (0, 30) (30, 55) (50, 55) (80, 70) (110, 25) (120, 50) };
            \draw (10,10) arc (270:290:1.4);
            \node[below left] at (0, 0) {$ 0 $};
            \coordinate (O) at (15, 43.5);
            \coordinate (A) at (120, 43.5);
            \coordinate (B) at (30, 55);
            \pic["$ \alpha $", draw, angle radius=0.8cm]{angle = A--O--B};
            \draw[line width=0.7pt] (O)--(40, 63.33);
            \draw[line width=0.7pt] (O)--(47.5, 43.5);
            \node at (O)[circle, fill, inner sep=1pt]{};
        \end{axis}
    \end{tikzpicture}
\end{center}

\noindent
La velocità istantanea, $ v(t) $, è data dalla pendenza punto per punto:

\begin{equation}
    v(t) = \lim_{\Delta t \rightarrow 0} \frac{s(t+\Delta t) - s(t)}{\Delta t} = \frac{ds}{dt} = s^{\prime}(t)
\end{equation}

\noindent
\newline
O anche:

\begin{equation}
    v(t) = tan(\alpha)
\end{equation}

\noindent
\newline
Le "dimensioni fisiche" sono $ lt^{-1} $ % ?

\bigskip
\begin{center}
    \begin{tikzpicture}[scale=0.8]
        \begin{axis}[
                axis x line = center,
                axis y line = center,
                ticks = none,
                xlabel = {$ t $},
                ylabel = {$ v $},
                xlabel style = {below right},
                ylabel style = {above left},
                xmin = 0, xmax = 120,
                ymin = -20, ymax = 70,
                enlargelimits = true
            ]
            \addplot[smooth] coordinates{ (0, 40) (20, 40) (40, -10) (60, 20) (80, -20) (110, 25) (120, 0) };
        \end{axis}
    \end{tikzpicture}
    \begin{tikzpicture}[scale=0.8]
        \begin{axis}[
                axis x line = center,
                axis y line = center,
                ticks = none,
                xlabel = {$ t $},
                ylabel = {$ v $},
                xlabel style = {below right},
                ylabel style = {above left},
                xmin = 0, xmax = 120,
                ymin = 0, ymax = 70,
                enlargelimits = true
            ]
            \addplot[smooth] coordinates{ (0, 0) (45, 35) (60, 30) (90, 50) (105, 45) (120, 55) };
        \end{axis}
    \end{tikzpicture}
\end{center}

\noindent
\newline
In modo del tutto analogo, definiamo l'accelerazione, $ a(t) $, come la derivata della curva $ v(t) $:

\begin{equation}
    a(t) = \lim_{\Delta t \rightarrow 0} \frac{v(t+\Delta t) - v(t)}{\Delta t} = \frac{dv}{dt} = v^{\prime}(t)
\end{equation}

\newpage
\noindent
\newline
Analizziamo dei casi particolari del moto unidimensionale:
\begin{enumerate}[label=\Alph*.]
    %%%%%
    % A %
    %%%%%
    \item
    \textbf{Velocità istantanea costante} $ v = v_{0} $ (moto rettilineo uniforme).
    
    La legge del moto si ottiene per integrazione, infatti:
    
    \begin{equation}
        s(t) = s_{0} + \int_{0}^{t} v\left(t^{\prime}\right) dt^{\prime} = s_{0} + \int_{0}^{t} v_{0} dt^{\prime} = s_{0}+v_{0}t
    \end{equation}

    \noindent
    \newline
    Controprova:

    \begin{equation}
        \frac{ds}{dt} = \frac{d}{dt}\left(s_{0} + v_{0} t\right) = v_{0}
    \end{equation}
    
    \noindent
    \newline
    Per questo moto si ha ovviamente:
    
    \begin{equation}
        a = \frac{dv_{0}}{dt} = 0
    \end{equation}

    \textbf{Equazioni} (leggi del moto):
    
    \begin{equation}
        s = s_0 + v_0t \rightarrow v_0t \quad \textnormal{se} \quad s_{0} = 0
    \end{equation}
    \begin{equation}
        a = 0 \quad t = \frac{(s - s_0)}{v_0}
    \end{equation}
    \begin{equation}
        v = v_0 = \frac{s - s_0}{t} \quad \textnormal{notare che } v(t) = \overline{v} \ \forall\ t
    \end{equation}
    %%%%%
    % B %
    %%%%%
    \item
    \textbf{Accelerazione costante} "$ a_0 $"

    Integriamo $ a $ per ottenere $ v $:
    
    \begin{equation}
        v(t) = v_0 + \int^t_0{a_0 + dt^{\prime}} = v_{0} + a_{0}t
    \end{equation}
    
    \noindent
    \newline
    Integriamo di nuovo per ottenere $ s(t) $:

    \begin{equation}
        s(t) = s_0 + \int^t_0{(v_0 + a_0t^{\prime})dt^{\prime}} = s_0 + v_0t + \frac{1}{2}a_0t^2
    \end{equation}
    
    \newpage
    \noindent
    \newline
    \textbf{Equazioni} (leggi del moto):
    
    \begin{equation}
        s = s_{0} + v_{0} t + \frac{1}{2} a_{0} t^{2} \quad v = v_{0} + a_{0}t
    \end{equation}
    
    \begin{equation}
        a_{0}=\frac{v-v_{0}}{t} \quad \rightarrow \quad t=\frac{v-v_{0}}{a_{0}}
    \end{equation}
    
    \begin{equation}
        s = s_{0} + \frac{1}{2}\left(v + v_{0}\right) t
    \end{equation}
    
    \begin{equation}
        v^{2}={v_{0}}^{2}+2 a_{0}\left(s-s_{0}\right)
    \end{equation}

    \noindent
    \newline
    Usando queste possiamo riscrive la $ s(t) $ per ricavare altre relazioni utili.

\end{enumerate}

\subsection{Moto in 2 o 3 dimensioni}

\begin{center}
    \begin{tikzpicture}
        \begin{axis}[
                width = 6cm,
                height = 3cm,
                axis x line = center,
                axis y line = center,
                ticks = none,
                xlabel = {$ x $},
                ylabel = {$ y $},
                xlabel style = {below right},
                ylabel style = {above left},
                xmin = 0, xmax = 90,
                ymin = 0, ymax = 40,
                enlargelimits = true
            ]
            \addplot[-latex, smooth] coordinates{ (20, 20) (30, 30) (40, 25) (50, 32.5) (60, 25) (70, 25) (80, 20) };
            \node at (20, 20)[circle, fill, inner sep=1pt]{};
        \end{axis}
    \end{tikzpicture}
\end{center}

\begin{equation}
    \vec{s}=(x, y)
\end{equation}

\begin{equation}
    \vec{s}(t) = (x(t), y(t)) = x(t)\hat{x} + y(t)\hat{y}
\end{equation}

\begin{equation}
    \vec{v}(t)=\left(\frac{d x}{d t}, \frac{d y}{d t}\right)=\frac{d x}{d t} \hat{x}+\frac{d y}{d t} \hat{y}
\end{equation}

\newpage
\noindent
\newline
Analizziamo un caso particolare: il moto ristretto ("vincolato") su una circonferenza (moto circolare):

\begin{center}
    \begin{tikzpicture}
        \begin{axis}[
                width = 6cm,
                height = 6cm,
                axis x line = center,
                axis y line = center,
                xtick = {0},
                ytick = {0},
                xticklabels = {},
                yticklabels = {},
                xlabel = {$ x $},
                ylabel = {$ y $},
                xlabel style = {below right},
                ylabel style = {above right},
                xmin = -40, xmax = 40,
                ymin = -40, ymax = 40,
                enlargelimits = true
            ]
            \draw (0, 0) circle [radius=40];
            \coordinate (O) at (0, 0);
            \coordinate (A) at (40, 0);
            \coordinate (B) at (28.28, 28.28);
            \pic[draw]{angle = A--O--B};
            \draw[-latex] (0, 0) -- ({40*cos(45)}, {40*sin(45)});
            \node at ({20*cos(22.5)}, {20*sin(22.5)}) {$ \alpha(t) $};
            \node[above left, rotate=45] at ({30*cos(45)}, {30*sin(45)}) {$ R $};
            \node at ({40*cos(45)}, {40*sin(45)})[circle, fill, inner sep=1pt]{};
        \end{axis}
    \end{tikzpicture}
\end{center}

\begin{equation}
    \left\{\begin{array}{l}{x=R \cos \alpha (t)} \\ {y=R \sin \alpha (t)}\end{array}\right.
\end{equation}

\noindent
\newline
Prendiamo in esame il caso in cui $ \alpha (t) = \omega t $ con $ \omega $ costante, ovvero il "periodo" di una rivoluzione, $ T $, è costante. In un periodo $ \alpha $ aumenta di $ 2 \pi $.

\noindent
\newline
\begin{minipage}{0.5\textwidth}
    \begin{tikzpicture}[scale=0.8]
        \begin{axis}[
                axis x line = center,
                axis y line = center,
                xtick = {30, 60, 90},
                ytick = {20, 40, 60},
                xticklabels = {$ T $, $ 2T $, $ 3T $},
                yticklabels = {$ 2\pi $, $ 4\pi $, $ 6\pi $},
                xlabel = {$ t $},
                ylabel = {$ \alpha $},
                xlabel style = {below right},
                ylabel style = {above left},
                xmin = 0, xmax = 130,
                ymin = 0, ymax = 90,
                enlargelimits = true
            ]
            \addplot[smooth] coordinates{ (0, 0) (30, 20) (60, 40) (90, 60) };
            \addplot[dashed] coordinates{ (0, 20) (30, 20) };
            \addplot[dashed] coordinates{ (60, 0) (60, 40) };
            \addplot[dashed] coordinates{ (0, 60) (90, 60) };
        \end{axis}
    \end{tikzpicture}
\end{minipage}\hfill
\begin{minipage}{0.5\textwidth}
    \begin{fleqn}
        \begin{equation}
            \alpha=\frac{2 \pi}{T} t
        \end{equation}
    \end{fleqn}
    \noindent
    \newline
    L'inverso del periodo si chiama frequenza $ f $
\end{minipage}

\begin{equation}
    f = 1/T \quad \textnormal{(dimensioni } t^{-1} \textnormal{)} 
\end{equation}

\begin{equation}
    \alpha = 2 \pi f t
\end{equation}

\newpage
\noindent
\newline
E si definisce velocità angolare $ \omega \equiv 2 \pi f $, $ \alpha = \omega t $, $ \omega = \frac{d\alpha}{dt} $ ha le dimensioni di $ rad \cdot s^{-1} $

\begin{equation}
    \left\{\begin{array}{l}{x(t)=R \cos \alpha(t)=R \cos (\omega t)} \\ {y(t)=R \sin \alpha(t)=R \sin (\omega t)}\end{array}\right.
\end{equation}

\begin{equation}
    \left\{\begin{array}{l}{v_{x}(t)=-\omega R \sin(\omega t)} \\ {v_{y}(t)=\omega R \cos (\omega t)}\end{array}\right. \Rightarrow v^{2}={v_{x}}^{2}+{v_{y}}^{2}=w^{2} R^{2}
\end{equation}
\begin{center}
    (La velocità è costante in modulo)
\end{center}

\noindent
\newline
Da cui:
\framebox[1.2\width]{$ \omega R = v $}

\noindent
\newline
Lo spazio percorso è:
\begin{equation}
    s(t) = R \alpha (t)
\end{equation}
\begin{center}
    (supposto $ s_{0} = 0 $ e $ \alpha_0 = 0 $)
\end{center}

\noindent
\newline
L'accelerazione è:

\begin{equation}
    \left\{\begin{array}{l}{a_{x}(t) = -\omega^2 R \cos \omega t } \\ {a_{y}(t) = -\omega^{2} R \sin \omega t}\end{array}\right. \Rightarrow a = \omega^{2} R=\frac{v^{2}}{R}
\end{equation}

\noindent
\newline
\begin{minipage}{0.5\textwidth}
    \begin{tikzpicture}
        \begin{axis}[
                width = 6cm,
                height = 6cm,
                axis x line = center,
                axis y line = center,
                xtick = {0},
                ytick = {0},
                xticklabels = {},
                yticklabels = {},
                xlabel = {$ x $},
                ylabel = {$ y $},
                xlabel style = {below right},
                ylabel style = {above right},
                xmin = -40, xmax = 40,
                ymin = -40, ymax = 40,
                enlargelimits = true
            ]
            \draw (0, 0) circle [radius=40];
            \coordinate (O) at (0, 0);
            \coordinate (A) at (40, 0);
            \coordinate (B) at (28.28, 28.28);
            \draw[-latex] (0, 0) -- ({40*cos(45)}, {40*sin(45)});
            \node[below left, rotate=45] at ({25*cos(45)}, {25*sin(45)}) {$ \vec{s} $};
            \draw[-latex] ({42*cos(45)}, {42*sin(45) - 3}) -- ({29*cos(45)}, {29*sin(45) - 3});
            \node[below] at ({40*cos(45)}, {40*sin(45) - 3}) {$ \vec{a} $};
            \draw[-latex] ({40*cos(45)}, {40*sin(45)}) -- ({20*cos(135) + 40*cos(45)}, {20*sin(135) + 40*sin(45)});
            \node[above right] at ({40*cos(60)}, {40*sin(60)}) {$ \vec{v} $};
        \end{axis}
    \end{tikzpicture}
\end{minipage}\hfill
\begin{minipage}{0.5\textwidth}
    \noindent
    \newline
    I vettori posizione, $ \vec{v} $ e $ \vec{a} $ sono costanti in modulo ma non in verso.
\end{minipage}

\vspace{0.5cm}
\noindent
Inoltre si vede che $ \vec{v} \cdot \vec{s} = \varnothing $ e $ \vec{v} \cdot \vec{a} = \varnothing $, ovvero la velocità è $ \perp $ alla posizione e alla accelerazione. L'accelerazione è diretta verso il centro.

\newpage
\subsection{Moto armonico, oscillazioni}

Prendiamo la sola $ x(t) $ (o $ y(t) $) e abbiamo:

\begin{equation}
    x(t) = R \cos \omega t
\end{equation}

\begin{equation}
    v(t) = -\omega R \sin \omega t
\end{equation}

\begin{equation}
    a(t) = -\omega^{2} R \cos \omega t
\end{equation}

\noindent
\newline
Quindi l'accelerazione è di segno opposto allo spostamento. Inoltre per il modulo della velocità si ottiene:

\begin{equation}
    \mid v \mid = \sqrt{{v_{x}}^2 + {v_{y}}^2} = \sqrt{(\omega R)^{2}(\cos^2 + \sin^{2})} = \omega R
\end{equation}

\newpage
\subsection{Moto balistico in 2 dimensioni}

\noindent
\newline
Prendiamo $ \vec{a} = (0, g) $ con il seguente sistema di riferimento:
\begin{tikzpicture}[baseline=1ex]
    \begin{axis}[
            scale only axis,
            height = 4ex,
            axis x line = center,
            axis y line = center,
            xtick = {0},
            ytick = {0},
            xticklabels = {},
            yticklabels = {},
            xlabel = {$ x $},
            ylabel = {$ y $},
            xlabel style = {below right},
            ylabel style = {above right},
            xmin = 0, xmax = 1,
            ymin = 0, ymax = 1,
            enlargelimits = true
        ]
    \end{axis}
\end{tikzpicture}

\noindent
\newline
Avremo:

\begin{equation}
    \left\{\begin{array}{l}{x = x_{0} + v_{0x}t} \\ {y = y_{0} + v_{0y} + \frac{1}{2} g t^{2} }\end{array}\right.
\end{equation}

\noindent
\newline
Caso particolare di:

\begin{equation}
    \left\{\begin{array}{l}{x = x_{0} + v_{0x}t + \frac{1}{2}a_{0x} t^{2}} \\ {y = y_{0} + v_{0y}t + \frac{1}{2}a_{0y} t^{2}}\end{array}\right.
\end{equation}

\begin{equation}
    \overline{s} = \overline{s}_{0} + \overline{v}_{0} t + \frac{1}{2} + \overline{a} t^{2}
\end{equation}

\noindent
\newline
Prendiamo in esame il caso classico del lancio di un corpo ad un certo angolo rispetto all'asse orizzontale:

\noindent
\newline
\begin{minipage}{0.5\textwidth}
    \begin{tikzpicture}[scale=0.8]
        \begin{axis}[
                width = 6cm,
                height = 4cm,
                axis x line = center,
                axis y line = center,
                ticks = none,
                xlabel = {$ x $},
                ylabel = {$ y $},
                xlabel style = {below right},
                ylabel style = {above left},
                xmin = 0, xmax = 100,
                ymin = 0, ymax = 50,
                enlargelimits = true
            ]
        \coordinate (O) at (0, 0);
        \coordinate (A) at (30, 0);
        \coordinate (B) at (30, 20);
        \pic[draw]{angle = A--O--B};
        \draw[-latex] (O) -- (B);
        \node[rotate=35] at (12, 15) {$ \overline{v}_{0} $};
        \node at (20, 6) {$ \alpha $};
        \node at (0, 0)[circle, fill, inner sep=1pt]{};
        \end{axis}
    \end{tikzpicture}
\end{minipage}\hfill
\begin{minipage}{0.5\textwidth}
    \begin{fleqn}
        \begin{equation}
            \overline{a} = (0, -g) = cost
        \end{equation}

        \begin{equation}
            \overline{v}_{0} = (v_{0x}, v_{0y}) = v_{0x}\hat{x} + v_{0y}\hat{y}
        \end{equation}

        \begin{equation}
            \overline{s} = (x, y)
        \end{equation}
    \end{fleqn}
\end{minipage}

\begin{equation}
    \overline{v}=(v_{0x}, v_{0y} - gt)
\end{equation}

\begin{equation}
    \overline{s}=(v_{0x}t, v_{0y}t - \frac{1}{2}gt^{2}) \Rightarrow x = v_{0x}t, \ y = \dots
\end{equation}

\noindent
\newline
Cerchiamo l'istante in cui si tocca il suolo:

\begin{equation}
    y=0 \quad \Rightarrow \quad v_{0}yt - \frac{1}{2}gt^{2} = 0 \quad \Rightarrow \quad t(v_{0y} - \frac{1}{2}gt^{2}) = 0
\end{equation}

\begin{equation}
    t = \frac{2v_{0y}}{g}
\end{equation}

\noindent
\newline
Quale distanza ha percorso (gittata)?

\begin{equation}
    x = \frac{2v_{0x}v_{0y}}{g} = \frac{2\mid v_{0} \mid^{2} \cos \alpha \sin \alpha}{g} = \frac{{v_{0}}^{2} \sin(2 \alpha)}{g}
\end{equation}

\newpage
\noindent
\newline
Dopo quanto tempo raggiungiamo il punto di massima altezza?

\begin{center}
    Si ha per $ v_{y} = 0 \Rightarrow $ \framebox[1.2\width]{$ t = v_{0y}/g $}
\end{center}

\noindent
\newline
Notare che essendo $ t = (x/v_{0x}) $ si ha:

\begin{equation}
    y = \frac{v_{0y}}{v_{0x}} x - \frac{1}{2} \frac{gx^2}{{v_{0x}}^{2}} \quad \Rightarrow \quad \textnormal{parabola, vertice } x
\end{equation}

\begin{equation}
    -b/2a = \frac{v_{0y}}{\cancel{v_{0x}}} \frac{{v_{0x}}^{\cancel{2}}}{g}
\end{equation}

\begin{equation}
    y_{max} = \frac{v_{0y}}{\cancel{v_{0x}}} \frac{v_{0y}\cancel{v_{0x}}}{g} - \frac{1}{2} \frac{{v_{0y}}^{2}}{g} = \frac{{v_{0y}}^{2}}{2g}
\end{equation}

\begin{center}
 \framebox[1.2\width]{$ {v_{0y}}^{2} = 2gy_{max} $} \ come se fosse in 1 dimensione.
\end{center}

\end{document}