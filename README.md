# Cos'è questo repository?
Una raccolta di appunti che possono essere utili a chi partecipa al corso di ingegneria e scienze informatiche della Unibo a Cesena.

# Perché è stato creato?
Per semplificare la collaborazione sugli appunti e per crearne una raccolta accessibile a tutti

# Dove trovo gli appunti?
Gli appunti sono disponibili in due formati:

1. In **[LaTeX](https://www.latex-project.org/)**, nella sezione **[Source](https://bitbucket.org/FrancoRighetti/unibo-cs-appunti/src/master/)**, per chi vuole il codice sorgente
2. In **PDF**, nella sezione **[Downloads](https://bitbucket.org/FrancoRighetti/unibo-cs-appunti/downloads/)**, per chi vuole la versione compilata

# Come faccio a contribuire?
Basta creare una **[pull request](https://www.atlassian.com/git/tutorials/making-a-pull-request)**. Lo strumento che consigliamo per lavorare su documenti LaTeX è **[Overleaf](https://www.overleaf.com/)**.

---

```Q:``` Perché Bitbucket?  
```A:``` Per questioni di comodità. C'è una buona probabilità che chi partecipa al corso a cui questo repository è mirato, sia già registrato a Bitbucket, rimuovendo il tedio di doversi registrare qualora si voglia fare una pull request.